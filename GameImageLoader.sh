#!/bin/bash
#
# Game image loader
#   Handles automatic mounting/unmounting of game disks prior to launch.
#
# Requirements:
#   cdemu (https://sourceforge.net/projects/cdemu/)
#   udisks (https://github.com/storaged-project/udisks)
#
# Installation:
#   1 Copy GameImageLoader to game directory (rename if you wish)
#   2 Modify GAMEEXE and GAMEDISK paths to desired game exe and image
#   3 Run
#   4 Have fun! :)
#
# Property of Andrew Wyatt, all rights reserved
# 


GAMEEXE="./Game.exe"
GAMEDISK="./Game.iso"

#####

checkRequirements () {
  hash cdemu || { echo "cdemu not installed, aborting."; return 1; }
  hash udisksctl || { echo "udisksctl not installed, aborting"; return 1; }
}

getTopDisk () { cdemu status | grep "^[0-9]" | sort -nr | tr -s ' ' | cut -d$'\n' -f 1 | cut -d' ' -f 1; }
setupDisk () { cdemu add-device >/dev/null; }

cleanupDisk () {
  if [ "$1" -gt 0 ]; then
    if [ "$1" -eq "$(getTopDisk)" ]; then
      cdemu remove-device
    fi
  fi
}

checkLoaded () {
  case "$(cdemu status | grep "^$1 " | tr -s ' ' | cut -d' ' -f 2)" in
    True) return 0 ;;
    False) return 1 ;;
    *) return 2 ;;
  esac

  echo "Error in checkLoaded function, case check failed!"
  exit 1
  }

selectDisk () {
  local deviceID
  local i=0
  local dosetup=1

  until [ ! "$i" \<  "$(getTopDisk)" ]; do
    checkLoaded $i

    case "$?" in
      1) break ;;
      2) dosetup=0; break ;;
      0) if [ "$i" -eq "$(getTopDisk)" ]; then dosetup=0; fi ;;
    esac

    i+=1
  done

  if [ "$dosetup" = 0 ]; then
    setupDisk
  fi

  echo $i

  }

doExit () {
  cdemu unload "$1"
  cleanupDisk "$1"
  exit "$2"
}

main () {
  local deviceID

  deviceID=$(selectDisk)
  cdemu load "$deviceID" "$GAMEDISK"
  sleep 2s
  udisksctl mount -b "/dev/sr$deviceID"
  wine "$GAMEEXE"
  doExit "$deviceID" $?
  return 0
  }

checkRequirements || exit 1
main && exit 0