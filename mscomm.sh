#!/bin/bash
#Minecraft Server Commander - b1.1
#
# This is an old scrcipt I wrote years ago that was (poorly xD) designed to handle
# minecraft server maintainence. 

#Configure various options
MINECRAFT_HOME="$HOME/VanillaPVE"
VER=1.7.9
MCJAR="minecraft_server.$VER.jar"
SCREEN_NAME="minecraft"
MINM=256M
MAXM=768M
EDIT="nano"
STAMP=$(date +"%m-%d-%Y")
FILE="backup.$STAMP.tar.gz"
arg2="$2"


#Configure local files (server.properties, whitelist.json, etc)
configmc () {
	local Mtype="$(echo ${VER:2:1})"
	if [ "$Mtype" -ge "6" ]; then
		case $arg2 in
			prop) $EDIT $MINECRAFT_HOME/server.properties ;;
			whitelist) $EDIT $MINECRAFT_HOME/whitelist.json ;;
			ban) $EDIT $MINECRAFT_HOME/banned-players.json ;;
			bip)  $EDIT $MINECRAFT_HOME/banned-ips.json ;;
			ops) $EDIT $MINECRAFT_HOME/ops.json ;;
			*) echo "Invalid Option. Use mscomm -h for help." ;; 
		esac
		exit 0 #heh
	else
		case $arg2 in
			prop) $EDIT $MINECRAFT_HOME/server.properties ;;
			whitelist) $EDIT $MINECRAFT_HOME/whitelist.txt ;;
			ban) $EDIT $MINECRAFT_HOME/banned-players.txt ;;
			bip)  $EDIT $MINECRAFT_HOME/banned-ips.txt ;;
			ops) $EDIT $MINECRAFT_HOME/ops.txt ;;
			*) echo "Invalid Option. Use mscomm -h for help." ;; #The answer to life
		esac
		exit 0
	fi
}



#Find the PID of Minecraft's screen session. Many thanks to stackexchange for this guy ;) 
minepid () { 
	mcpid=$(screen -ls | awk '/\.minecraft\t/ {print strtonum($1)}')
}


#Check whether or not minecraft is already running
minecheck () {	
	if ! screen -list | grep -q "minecraft"; then
		echo "1"
	else
		echo "0"
	fi	
}

#Prints the pid
findpid () {
	if [ `minecheck` == 0 ]; then
		minepid
		echo $mcpid
	else
		echo "null"
	fi
}


#Brings it all together
SERVER () {
	screen -dmS $SCREEN_NAME java -Xms$MINM -Xmx$MAXM -jar $MINECRAFT_HOME/$MCJAR nogui
	if [ `minecheck` == 1 ]; then
		echo "Failed to start server."
	else
		echo "Server started successfully."
	fi
}


#Bit self explanatory, but it starts the minecraft server
startmc () {
	cd $MINECRAFT_HOME
	if [ `minecheck` == 1 ]; then
		echo `SERVER`
	else
		echo "$SCREEN_NAME is already running. Use screen -r `findpid` to view."
	fi
}


#Again, self explanatory, but it attempts to stop the server twice, before just killing the screen.
stopmc () {
	if [ `minecheck` == 1 ]; then
		echo "No Minecraft server running"
	else
		minepid
		screen -S $mcpid -X detach
		screen -S $mcpid -X stuff "`printf 'save-all\r'`"
		echo "Stopping Minecraft server in 10 seconds."
		screen -S $mcpid -X detach
		screen -S $mcpid -X stuff "`printf 'say Stopping server in 10 seconds.\r'`"
		sleep 10
		echo "Stopping Minecraft server..."
		screen -S $mcpid -X detach
		screen -S $mcpid -X stuff "`printf 'say Stopping server....\r'`"
		sleep 1
		screen -S $mcpid -X detach
		screen -S $mcpid -X stuff "`printf 'stop\r'`"
		sleep 15
		if [ `minecheck` == 0 ]; then
			echo "Shutdown failed, Retrying..."
			screen -S $mcpid -X stuff "`printf '\r'`"
			sleep 15
			screen -S $mcpid -X stuff "`printf 'stop\r'`"
			if [ `minecheck` == 0 ]; then
				echo "Shutdown failed, Killing screen..."
				kill $mcpid
				if [ `minecheck` == 0 ]; then
					echo "Shutdown failed, Killing java..."
					killall java
				else
					echo "Shutdown successful."
				fi
			else
				echo "Shutdown successful."
			fi
		else
			echo "Shutdown successful."
		fi
				
	fi
}


#Kills server.
killmc () {
	if [ `minecheck` == 1 ]; then
		echo "No Minecraft server running"
	else
		minepid
		screen -S $mcpid -X detach
		screen -S $mcpid -X stuff "`printf 'save-all\r'`"
		echo "Killing Minecraft server..."
		screen -S $mcpid -X detach
		screen -S $mcpid -X stuff "`printf 'save-all\r'`"
		sleep 5
		screen -S $mcpid -X detach
		screen -S $mcpid -X stuff "`printf 'stop\r'`"
		sleep 15
		if [ `minecheck` == 0 ]; then
			echo "Shutdown failed, Retrying..."
			screen -S $mcpid -X stuff "`printf '\r'`"
			sleep 15
			screen -S $mcpid -X stuff "`printf 'stop\r'`"
			if [ `minecheck` == 0 ]; then
				echo "Shutdown failed, Killing screen pid..."
				kill $mcpid
				if [ `minecheck` == 0 ];then
					echo "Shutdown failed, Killing java"
					killall java
				else
					echo "Shutdown successful"
					exit 0
				fi
			else
				echo "Shutdown successful."
				exit 0
			fi
		else
			echo "Shutdown successful."
			exit 0
		fi
		exit 0		
	fi
}



#Backs up minecraft, then restarts it, or leaves it off.
backupmc () {
	if [ `minecheck` == "1" ]; then
		echo "Starting backup of $MINECRAFT_HOME"
		tar cf ~/$FILE $MINECRAFT_HOME/*
		echo "Backup location: ~/$FILE"
		exit 0
	else
		stopmc
		sleep 5
		echo "Starting backup of $MINECRAFT_HOME"
		tar cf ~/$FILE $MINECRAFT_HOME/*
		echo "Backup location: ~/$FILE"
		startmc
	fi
}


#Aquires the status of the minecraft instance, as well as lists current configs
status () {
	clear
	local RUNNING=$( if [ `minecheck` == 1 ]; then echo "NO"; else echo "YES"; fi )
	echo "--Current Configuration-- "
	echo "Minecraft Version:	| $VER"
	echo "Minecraft Directory:	| $MINECRAFT_HOME"
	echo "Jar Name:		| $MCJAR"
	echo "Minimum Memory:		| $MINM"
	echo "Maximum Memory:		| $MAXM"
	echo "Current Editor:		| $EDIT"
	echo "Online:			| $RUNNING"
	if [ $RUNNING == "YES" ];then
		echo "PID:			| `findpid`"
	fi
	exit 0
}


#Connects user to screen socket
connectmc () {
	if [ `minecheck` == 0 ]; then
		screen -r `findpid`
	else
		echo "No server Detected"
	fi
	exit 0
}


#Obligatory help text :P
helptext () {
	clear
	echo "Minecraft Server Commander - b1.0"
	echo "mscomm: mscomm [options]"
	echo ""
	echo "    Can start,stop,backup,and acquire the status of a minecraft server instance it controls."
	echo ""
	echo "    Options:"
	echo "		-T | stop		Attempts to stop the server gracefully with plenty of warning"
	echo "		-k | kill		Kills the server"
	echo "		-s | start		Starts a minecraft server instance under gnu screen"
	echo "		-S | status		Queries the status, and the current configuration of the server and this script"
	echo "		-p | pid		Prints the pid of the instance, if it's running"
	echo "		-c | check		Prints 0 or 1 depending on if the instance is running or not"
	echo "		-h | help		This help text"
	echo "		-f | config		Edits specified config file using nano"
	echo ""
	echo "	   Usage:"
	echo "		config:  	mscomm config [prop,whitelist,ops,ban,bip]"
	echo "			prop      = server.properties"
	echo "			whitelist = whitelist"
	echo "			ops       = ops"
	echo "			bans	  = banlist"
	echo "			bip       = IP ban list"
	echo ""
	exit 0
}


#Arguments. Duh.
case $1 in
	stop|-T) stopmc && exit 0; exit 1 ;;
	kill|-k) killmc && exit 0; exit 1 ;;
	backup|-b) backupmc && exit 0; exit 1;;
	start|-s) startmc && exit 0; exit 1 ;;
	pid|-p) findpid && exit 0; exit 1 ;;
	check|-c) minecheck|echo `minecheck` && exit 0; exit 1 ;;
	status|-S) status || exit 1 ;;
	connect|-C) connectmc || exit 1 ;;
	config|-f) configmc || exit 1 ;;
	help|-h|*) helptext || exit 1 ;;
esac
