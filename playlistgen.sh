#!/bin/bash
#
# Generates a playlist file compatible with cmus when pointed at a directory

plName="$2"
workDir="$1"
currentDir=$PWD
main () {
    cd "$workDir"
    echo "Generating $plName"
    if [[ -f "$currentDir/$plName" ]]; then rm "$currentDir/$plName" && echo "Overwriting..."; fi
    for fileName in *; do
        printf "$workDir/$fileName\n" >> "$currentDir/$plName"
    done
}

main
