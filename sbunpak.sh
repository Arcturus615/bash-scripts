#!/bin/bash
#
# This is another modding tool I wrote that unpacks Starbound mod files

UNPAKER="/media/SSDStorage/Games/Linux/Steam/steamapps/common/Starbound/linux/asset_unpacker"
DEFAULTLOC="$HOME/unpacked"
RUNLOC="$(pwd)"

unpackfile () {
    local _file
    local _filename
    local _outloc

    _file=$(basename "$1") || exit 1
    _filename=${_file%.*}
    if [ "$2" == "" ]; then
        mkdir -p "$DEFAULTLOC/$_filename"
        _outloc="$DEFAULTLOC/$_filename"
    else
        mkdir -p "$2/$_filename"
        _outloc="$2/$_filename"
    fi
    $UNPAKER "$1" "$_outloc"
}

unpackdir () {
    local _file
    local _filename
    local _outloc

    if [ "$2" == "" ]; then
        mkdir -p "$HOME/unpacked"
        _outloc="$HOME/unpacked"
    else
        mkdir -p "$2" || exit 1
        cd "$2" || exit 1
        _outloc="$(pwd)"
        cd "$RUNLOC" || exit 1
    fi

    cd "$1" || exit 1
    for f in *.pak; do
        _file=$(basename "$f") || exit 1
        _filename=${_file%.*}
        unpackfile "$f" "$_outloc"
    done
}

helptext () {
	cat <<- __EOF
		Usage: sbunpak [argument] [input file/folder] [output folder]

		Options:
			-f or --file            Upacking a file
			-d or --directory       Unpack every file in a directory
			-h or --help            This help text

		When unpacking, if no output is specified then "$HOME/unpacked" is used as output
	__EOF
}

case $1 in
    --directory|-d) unpackdir "$2" "$3" && exit 0; exit 1 ;;
    --file|-f) unpackfile "$2" "$3" && exit 0; exit 1 ;;
    --help|-h|*) helptext || exit 1 ;;
esac

exit 0
