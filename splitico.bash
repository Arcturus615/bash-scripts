#!/bin/bash
# Splitico
# 
# A tool for splitting MS .ico files into .png
# for easy inclusion in the local icon cache

hash convert  || { echo "Error: convert is not present on this system." ; exit 1 ;}
hash identify || { echo "Error: identify is not present on this system." ; exit 1 ;}

usagetext () { echo "Usage: splitico <inputfile> [parameters]";}
versiontext () { echo "Version: 1.0.0"; }
getscales () { identify "$1" | cut -d' ' -f 3;}
getfilename () { _file=$(basename "$1") || exit 1; echo "${_file%.*}"; }

helptext () {
    cat <<- "EOF"
		Command line tool for converting windows .ico files into multiple .png files

		Arguments
			-h	This help text
			-v	Version text

		File Parameters
			-o	Specify the name used by output files (default: <filename>.png)
			-f 	Specify the folder the converted files are placed in (default: ./icons)
			-c	Specify the category folder the files should be placed into (default: apps)
	EOF
}


if ! [ -f "$1" ] && ! [ "$(file --mime-type "$1")" == "image/x-icon" ]; then
    echo "Error: File \"$1\" doesn't exist or is invalid!"
    usagetext; exit 1
fi

_ifilepath="$1"
_ofoldername="./icons"
_ofilename="$(getfilename "$_ifilepath")"
_ocategory="apps"
shift

while getopts ":hvuo:f:c:" opt; do
    case "$opt" in
        c)  _ocategory=${OPTARG}    ;;
        f)  _ofoldername=${OPTARG}  ;;
        o)  _ofilename=${OPTARG}    ;;
        u)  usagetext; exit 0       ;;
        v)  versiontext; exit 0     ;;
        h)  helptext; exit 0        ;;
        ?)  usagetext; exit 0		;;
    esac
done

readarray -t scales < <(getscales "$_ifilepath")

_layer=0
for i in "${scales[@]}"; do
    mkdir -p "$_ofoldername/$i/$_ocategory"
    convert -thumbnail "$i" -alpha on -background none "$_ifilepath[$_layer]" "$_ofoldername/$i/$_ocategory/$_ofilename.png"
    ((_layer++))
done