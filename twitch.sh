#!/bin/bash
# Run livestreamer for specified stream

streamer=$1
quality=high
ringBuffer=32M
webBrowser=firefox

checkStream () { livestreamer twitch.tv/$streamer >/dev/null 2>&1 || return 1; }

helpText () {
    echo "Usage:        twitch [streamer] [parameters]"
    echo "Parameters:   --chat, --[best,high,medium,low]"
}

processParams () {
    case $* in
        *"--chat"*)     echo "Launching Chat..."
                        case $webBrowser in
                            midori)             midori -a "www.twitch.tv/$streamer/chat" >/dev/null 2>&1 &              ;;
                            firefox)            firefox "www.twitch.tv/$streamer/chat" >/dev/null 2>&1 &                ;;
                            chromium|chrome)    chromium-browser --app="www.twitch.tv/$streamer/chat" >/dev/null 2>&1 & ;;
                            luakit)             luakit -u "www.twitch.tv/$streamer/chat" >/dev/null 2>&1 &              ;;
                        esac
                                                                              ;;&
        *"--best"*)     echo "Forcing best quality";      quality="best"      ;;
        *"--high"*)     echo "Forcing high quality";      quality="high"      ;;
      *"--medium"*)     echo "Forcing medium quality";    quality="medium"    ;;
         *"--low"*)     echo "Forcing low quality";       quality="low"       ;;
    esac
}

failStream () {
    echo "$streamer is not streaming"
    notify-send "Livestreamer" "$streamer is not streaming"
    exit 0
}

startStream () {
    notify-send "Livestreamer" "Streaming $streamer"
    echo "Executing: livestreamer --ringbuffer-size $ringBuffer twitch.tv/$streamer $quality"
    livestreamer -p mpv --ringbuffer-size $ringBuffer twitch.tv/$streamer $quality >/dev/null 2>&1 &
    exit 0
}

if [ -z "$1" ] ; then helpText
else
    checkStream \
        && processParams $* \
        && startStream $1 \
        || failStream
fi
exit 1
